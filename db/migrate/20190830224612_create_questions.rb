class CreateQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :questions do |t|
      t.string :label1
      t.string :value1
      t.string :label2
      t.string :value2
      t.string :question

      t.timestamps
    end

    Question.create(label1: 'response1',label2: 'response2',value2: 'response1',value1: 'response1',question: 'test')
  end
end
